/*eslint no-console: ["error", { "allow": ["warn", "error"] }]*/
const fs=require("fs");
const openpgp=require("openpgp");

/*
 * Please, note that the var names are with inverted camelCase, socketPath is SOCKETpATH
 * so we follow the uppercase var names convention.
 */
const allowedEnv=["MYSQL_USER", "MYSQL_PASSWORD", "MYSQL_HOST", "MYSQL_PORT", "MYSQL_DATABASE", "MYSQL_SOCKETpATH", "OPENPGP_PASSPHRASE"];
function swapCase(text){
    return text.split('')
        //eslint-disable-next-line no-confusing-arrow
        .map(c=>c === c.toUpperCase()?c.toLowerCase():c.toUpperCase())
        .join('');
}

async function readCredentials(){
    let config=await new Promise((r,j)=>{
        fs.readFile(".privado/credenciales.json",(e,s)=>{
            if(e)return j(e);
                    return r(s);
        });
    }).then(file=>{
        return JSON.parse(file);
    }).catch(e=>{
        if(e.code=="ENOENT"){
            return decryptCredentials().then(text=>JSON.parse(text));
        }
    });
    return replaceWithEnv(config);
}
function replaceWithEnv(config){
    for(let uncased of allowedEnv){
        let cfg=config;
        if(!process.env[uncased])continue;
        let prop=swapCase(uncased);
        let parents=prop.split("_");
        let name=parents.pop();
        do{
            let actual=parents.splice(0,1);
            cfg=cfg[actual];
        }while(parents.length>0);
        if(process.env[uncased]){
            cfg[name]=process.env[uncased];
        }
    }
    return config;
}
async function decryptCredentials(file=".privado/credenciales.json.gpg"){
    let message=await openpgp.message.read(fs.readFileSync(file));
    if(!process.env.ENCRYPTION_KEY){
        console.warn("No ENCRYPTION_KEY nor decrypted credentials. Using an empty object as base");
        return JSON.stringify({mysql:{}, openpgp: {}});
    }
    let passwords=[process.env.ENCRYPTION_KEY];
    return openpgp.decrypt({message,passwords}).then(text=>text.data);
}

module.exports={readCredentials};
