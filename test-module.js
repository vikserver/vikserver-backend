const fs=require("fs");
const path=require("path");

function moduleTest(verbose){
    global.verbose=verbose;
    let dependencias=(function(){
        return JSON.parse(fs.readFileSync("package.json")).dependencies;
    })();
    Object.keys(dependencias).forEach(a=>{
        try{
            if(require(a)==undefined){
                throw "Node module "+a+" is not installed and is required";
            }else{
                Console.log("Module "+a+" is correctly installed");
            }
        }catch(e){
            Console.log(e);
            throw new Error("Some NodeJS modules are not installed: ",a);
        }
    });
    return true;
}
function codeTest(files,dirs,verbose){
    global.verbose=verbose;
    const babel=require("@babel/core");
    var tests={
        JS(pattern){
            var errors=[];
            var files=0; //File and error count
            dirs.forEach(d=>{
                let list=fs.readdirSync(d).filter(a=>a.match(pattern));
                list.forEach(f=>{
                    let file=path.resolve([d, f].join("/"));
                    Console.log(`Testing ${file}`);
                    try{
                        babel.transform(fs.readFileSync(file, "UTF-8"));
                        Console.log(`${file} syntax correct`);
                    }catch(e){
                        Console.error("Syntax incorrect on "+file, e.loc);
                        e.path=file;
                        Console.log(e.message);
                        errors.push(e);
                    }
                    files++;
                });
            });
            return {files,errors};
        },
        JSON(pattern){
            var files=0;
            var errors=[];
            dirs.forEach(d=>{
                let list=fs.readdirSync(d).filter(a=>a.match(pattern));
                list.forEach(f=>{
                    let file=path.resolve([d,f].join("/"));
                    Console.log(`Testing ${file}`);
                    try{
                        let tst=JSON.parse(fs.readFileSync(file, "UTF-8"));
                        if(typeof tst!="object")throw new Error(`${file} parsed is not an object`);
                    }catch(e){
                        Console.error("Incorrect JSON on "+file);
                        e.path=file;
                        Console.log(e.message);
                        errors.push(e);
                    }
                    files++;
                });
            });
            return {files,errors};
        }
    };
    for(let i in files){
        if(!files.hasOwnProperty(i))continue;
        let res=tests[i](files[i]);
        let method=res.errors.length>0?"error":"log";
        
        Console[method](`Tested ${res.files} ${i} files. ${res.errors.length} error${res.errors.length==1 ? '' : 's'}`);
        //When running on mocha, throw error instead of exit process.
        if(res.errors.length>0){
            if(typeof it=="function"&&typeof describe=="function")throw new Error("One or more errors during basic test");
            process.exit(res.errors.length);
        }
    }
}

var Console={ 
    log: function(...args){
        let verbose=getvb();
        if(verbose)return console.log(...args); //eslint-disable-line no-console
    },
    warn: function(...args){
        let verbose=getvb();
        if(verbose==false)return;
        return console.warn(...args); //eslint-disable-line no-console
    },
    error: function(...args){
        return console.error(...args); //eslint-disable-line no-console
    }
}
function getvb(){
    return typeof global.verbose=="undefined"?process.env.NODE_ENV!="production":global.verbose;
}

module.exports={moduleTest,codeTest};
