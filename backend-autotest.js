#!/bin/env node
/*eslint-disable no-console*/
const io=require("socket.io-client");
function testRunningBackend(at="http://localhost:"+(process.env.PORT||10000)){
    const socket=io(at, {autoConnect:false});
    return new Promise((resolve,reject)=>{
        let timeo=setTimeout(()=>reject(new Error("Connection timeout exceeded")),10000);
        socket.on("err",reject);
        socket.on("error",reject);
        socket.on("connect_error",reject);
        socket.on("connect", ()=>{
            clearTimeout(timeo);
            console.log("Conected to backend OK");
        });
        socket.open();
        let timeo2=setTimeout(reject,20000);
        socket.on("chk2", res=>{
            console.log("Recevied chk");
            clearTimeout(timeo2);
            socket.off("chk2");
            return resolve(res); //The user may not exist, but if the backend returns something, it's alive
        });
        socket.emit("chk",{msg:"someuser"});
    });
}
if(require.main===module){ //So it's been called directly
    testRunningBackend()
        .then(res=>{
            console.log(`Check done, user ${res?'exists':'not exist'}`);
            process.exit(0);
        })
        .catch(e=>{
        console.error(e);
        process.exit(1);
    });
}else{
    module.exports=testRunningBackend;
}
