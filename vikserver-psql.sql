\set SQL_MODE  "NO_AUTO_VALUE_ON_ZERO"
\set AUTOCOMMIT  false
START TRANSACTION;
\set time_zone "+00:00"

-- Tables --

CREATE TABLE "usuarios" (
  "id" SERIAL NOT NULL PRIMARY KEY,
  "nombre" varchar NOT NULL UNIQUE,
  "db" text NULL,
  "privateKey" text NOT NULL,
  "publicKey" text NOT NULL,
  "modificado" bigint NOT NULL
);

CREATE TABLE "Claves" (
  "id" varchar NOT NULL PRIMARY KEY,
  "private" text NOT NULL,
  "public" text NOT NULL,
  "modificado" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "misc" (
  "clave" varchar NOT NULL PRIMARY KEY,
  "valor" varchar NOT NULL,
  "actualizada" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "short" (
  "id" SERIAL NOT NULL,
  "link" text NOT NULL,
  "uid" varchar NOT NULL UNIQUE,
  "usuario" varchar NOT NULL REFERENCES usuarios(nombre),
  "modificado" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (uid)
);

-- Indexes --
CREATE INDEX usernames ON "usuarios" (nombre);
CREATE INDEX shortlinkusers ON "short" (usuario, uid);

-- Functions --
--  CREATE LANGUAGE "plpgsql";

CREATE OR REPLACE FUNCTION update_modificado()
RETURNS TRIGGER AS $$
BEGIN
    NEW.modificado=now();
    RETURN NEW;
END;
$$ language 'plpgsql';

-- Triggers --
CREATE TRIGGER update_short_modified BEFORE UPDATE
ON "short" FOR EACH ROW EXECUTE PROCEDURE
update_modificado();

CREATE TRIGGER update_Claves_modified BEFORE UPDATE
ON "Claves" FOR EACH ROW EXECUTE PROCEDURE
update_modificado();

END TRANSACTION;
