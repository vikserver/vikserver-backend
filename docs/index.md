---
title: "Vikserver backend docs"
permalink: "/"
---

# Vikserver-backend docs

### Welcome to the documentation
This docs are intended to help contributors understand this project. As many things are applied to vikserver-backend, remember this requires a frontend, so the docs are shared between this two projects. Enjoy

### Index
- [First steps](/first-steps)
- [Functions explanation](/functions)
- [Socket.io events](/socket.io-events)
