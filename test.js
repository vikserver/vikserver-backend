"use-strict";
const {codeTest,moduleTest}=require("./test-module");

if(typeof process !="undefined"){
    var verbose=true;
    if(process.env.NODE_ENV=="production")verbose=false;

    let patterns={
        JS: /^.*\.js$/,
        JSON: /^.*\.json$/
    };
    let directories=[__dirname||".", "test"];
    moduleTest(verbose);
    codeTest(patterns,directories,verbose);
}else{
    throw new Error("This is a backend software and must be run from Node.js");
}
