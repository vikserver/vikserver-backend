FROM node:alpine

ENV NODE_ENV production
ENV PORT 8080

RUN apk --update upgrade&&apk add git shadow

USER node

COPY --chown=node:node . /vikserver-backend

WORKDIR /vikserver-backend

RUN npm install

EXPOSE 8080/tcp

CMD ["npm", "start"]
