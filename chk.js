/*eslint-disable no-console*/

const fs=require("fs");
const path=require("path");
const {readCredentials}=require("./credential-helper");
console.log("Starting test...");
async function checkPlatform(){
    var platforms=["linux"];
    var arch=["x64"];
    var untrusted="";
    if(!platforms.includes(process.platform)){
        untrusted+="platform";
    }
    if(!arch.includes(process.arch)){
        if(untrusted!="")untrusted+=" and ";
        untrusted+="architecture";
    }
    if(untrusted!="")console.log(`NOTICE: Running on ${process.platform} over a ${process.arch}. This ${untrusted} is untrusted. Please, send reports`);
}
async function checkCode(){
    const {codeTest}=require("./test-module");
    let verbose=true;
    if(process.env.NODE_ENV=="production")verbose=false;
    let patterns={
        JS: /^.*\.js$/,
        JSON: /^.*\.json$/
    };
    let directories=[__dirname||"."];
    codeTest(patterns,directories,verbose);
}
async function checkDatabase(creds){
    const mysql=require("mysql");
    creds.mysql["multipleStatements"]=true;
    console.log("Connecting to MySQL database");
    var pool=mysql.createPool(creds.mysql);
    return new Promise((resolve, reject)=>{
        pool.query("SELECT 1+1 AS solucion", err=>{
            if(err){
                console.error("Database rejected a generic query. Cannot continue");
                return reject(err);
            }
            console.log("Database connected");
            return resolve(pool);
        });
    });
}
async function checkEntries(pool){
    return new Promise((resolve,reject)=>{
        pool.query("SELECT * FROM test", err=>{
            if(err){
                console.log("Updating database...");
                let file=path.join(__dirname,"vikserver.sql");
                return pool.query(fs.readFileSync(file, "utf8"), (err, data)=>{
                    if(err){
                        console.error("Couldn't update the database");
                        return reject(err);
                    }
                    console.log("Updated database successfully");
                    return resolve(data);
                });
            }
            console.log("Database was up to date");
            return resolve(true);
        });
    }).then(data=>{
        if(process.env.NODE_ENV=="test"){
            let file=fs.readFileSync(path.join(__dirname,"vikserver-testdb.sql"),"utf8");
            return new Promise((resolve,reject)=>{
                return pool.query(file,(err,dt)=>{
                    if(err)return reject(err);
                    return resolve(dt);
                })
            });
        }
        return data;
    })
}
(async function(){
    var creds;
    {
        console.log("Doing platform check");
        await checkPlatform();
    }
    {
        console.log("Checking syntax on the scripts");
        await checkCode();
    }
    {
        console.log("Reading credentials");
        creds=await readCredentials();
    }
    {
        console.log("Checking database connectivity and consistency");
        let pool=await checkDatabase(creds);
        await checkEntries(pool).then(()=>{
            return pool.end();
        });
    }
})()
.then(status=>exit(status))
.catch(e=>error(e));
function exit(code=0){
    console.log("Checking finished. Exiting with code "+code);
    if(code>0)process.exit(code);
}
function error(e){
    console.error(e);
    return exit(1);
}
