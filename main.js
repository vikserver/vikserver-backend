/*eslint-disable multiline-comment-style, spaced-comment*/
/*eslint no-console: ["error", { "allow": ["warn", "error"] }]*/
/**
 * @author MrViK
 * @description Vikserver2 backend using WebSockets
 * @fileoverview Main Vikserver backend file
 */

class Console{
    static log(...args){
        let env=process.env.NODE_ENV;
        //eslint-disable-next-line no-console
        if(!env||env.match("dev")||env.match("prod"))return console.log(...args);
        return 0;
    }
}

//-------------------------------------
// REQUIREMENTS
//-------------------------------------

Console.log("Starting backend...");
const fs=require("fs");
const {readCredentials}=require("./credential-helper");
const mysql=require("mysql");
const SqlString=require("sqlstring");
const openpgp=require("openpgp");
const {IncomingWebhook}=require("@slack/client");
const {createServer}=require("http");
const socketIO=require("socket.io");
const {StringDecoder}=require("string_decoder");
var creds;
var credsProgress=readCredentials().then(c=>{
    creds=c
});

//-------------------------------------
// MAIN FUNCTIONS
//-------------------------------------
//MySQL connection
var pool;
var mysqlProgress=(async function(){
    await credsProgress;
    pool=mysql.createPool(creds.mysql);
    pool.query("SELECT * FROM test", err=>{
        if(err) throw err;
        Console.log("Conectado a MySQL con éxito");
    });

})();
async function query(a){
    await mysqlProgress;
    let max_tries=4;
    var err;
    for(let tries=0; tries<max_tries; tries++){
        try{
            let conn=await new Promise((resolver,rechazar)=>{
                pool.getConnection((e,c)=>{
                    if(e)return rechazar(e);
                    return resolver(c);
                });
            });
            let X=await new Promise((resolver,rechazar)=>{
                conn.query(a, (err, res, campos)=>{
                    if(err) return rechazar(err);
                    resolver({res: res, campos: campos});
                });
            }).finally(()=>{
                conn.release();
            })
            return X;
        }catch(e){
            err=e;
            continue;
        }
    }
    console.error("Max tries exceeded while connecting to MySQL");
    throw err;
}

//OpenPGP initialization
var pgpKeys,
    pgpObj;
openpgp.initWorker("node_modules/openpgp/dist/openpgp.worker.min.js");
const pgpProgress=(async function(){
    let a= await query("SELECT * FROM Claves WHERE id='WebSocket'");
    if(a.res.length==1){
        try{
            Console.log("Key pair received from database");
            pgpKeys={
                pública: a.res[0].public,
                privada: a.res[0].private
            };
            pgpObj={
                pública: (await openpgp.key.readArmored(a.res[0].public)).keys,
                privada: (await openpgp.key.readArmored(a.res[0].private)).keys[0]
            }
            return pgpObj.privada.decrypt(creds.openpgp.passphrase).then(()=>{
                Console.log("PGP key decrypted");
            })
        }catch(e){
            console.error("Error while decrypting OpenPGP key");
            throw e;
        }
    }else{
        try{
            Console.log("Generating new key...");
            let key=await openpgp.generateKey(creds.openpgp);
            Console.log("Keypair successfully generated");
            pgpKeys={
                pública: key.publicKeyArmored,
                privada: key.privateKeyArmored
            };
            pgpObj={
                pública: (await openpgp.key.readArmored(pgpKeys.pública)).keys,
                privada: (await openpgp.key.readArmored(pgpKeys.privada)).keys[0]
            }
            await pgpObj.privada.decrypt(creds.openpgp.passphrase).then(()=>{
                Console.log("PGP key decrypted");
            })
            .catch(e=>{
                console.error("Incorrect password for PGP key");
                throw e;
            });
            return query("INSERT INTO Claves (id, public, private) VALUES ('WebSocket', '"+pgpKeys.pública+"', '"+pgpKeys.privada+"')").then(()=>{
                Console.log("Keypair stored successfully in database");
            });
        }catch(e){
            console.error("There was an error while inserting values in database");
            throw e;
        }
    }
})().catch(e=>{
    e.fatal=true; //With an error in this part, the backend cannot work.
    throw e;
});

//Socket connections handling
const defaultPage=fs.readFileSync("index.html","UTF-8");
const server=createServer((req,res)=>{
    if(!req.url.match("socket.io")){
        res.writeHead(200, {"Content-Type": "text/html"});
        res.write(defaultPage);
        res.end();
    }
});
const io=socketIO(server);
var port=process.env.PORT || 10000;

const portListenProgress=(async function(){
    await pgpProgress;
    try{
        server.listen(port, ()=>{
            Console.log("Servidor WEB iniciado en el puerto "+port);
        });
    }catch(e){
        console.error("Error while starting server!");
        e.fatal=true;
        throw e;
    }
    io.on("connection", socket=>{
        socket.emit("pgp", {name: "WebSocketServer", key: pgpKeys.pública});
        socket.on("chk", async a=>{
            let msg=SqlString.escape(a.msg);
            let q=await query(`SELECT nombre FROM usuarios WHERE nombre=${msg}`);
            if(q.res.length==0){
                return socket.emit("chk2", false);
            }
            let r=q.res.filter(c=>c.nombre==a.msg);
            if(r[0])return socket.emit("chk2", true);

            socket.emit("chk2", false);
        });
        socket.on("req-pgp", async a=>{
            let b=await desencriptar(a.msg);
            let c=JSON.parse(b.data);
            let usuario=SqlString.escape(c.usuario);
            await query(`SELECT * FROM usuarios WHERE nombre=${usuario}`).then(async d=>{
                if(d.res.length<1){
                    socket.emit("req-pgp2", false);
                    socket.emit("direct", "pgp_404");
                    return;
                }
                let r=d.res.filter(f=>f.nombre==c.usuario)[0];
                if(r){
                    return (await openpgp.key.readArmored(r.privateKey)).keys[0].decrypt(c.contraseña).then(dec=>{
                        if(!dec)throw new Error("Incorrect key");
                        return socket.emit("req-pgp2", {usuario: c.usuario, pgp:{privada: r.privateKey, pública: r.publicKey}});
                    })
                    .catch(e=>{ //This is a error who shall not be raised
                        const {message,stack}=e;
                        socket.emit("debug",JSON.stringify({message,stack}));
                        return socket.emit("req-pgp2", false);
                    });
                }
            })
            .catch(e=>{
                socket.emit("direct", "auth_error"); //Warn user
                throw e; //Rethrow error
            })
        });
        socket.on("registro", a=>{
            return desencriptar(a.msg).then(async b=>{
                let c=JSON.parse(b.data);
                let usuario=SqlString.escape(c.creds.usuario);
                let q1=`SELECT nombre FROM usuarios WHERE nombre=${usuario}`;
                let q1r=await query(q1).then(X=>X.res.length);
                if(q1r){
                    socket.emit("registro2", false);
                    return socket.emit("direct", "user_exists");
                }
                let q=`INSERT INTO usuarios (nombre,privateKey,publicKey,modificado) VALUES (${usuario},'${c.keys.privada}','${c.keys.pública}','${Date.now()}')`;
                return query(q);
            }).then(()=>{
                socket.emit("registro2", true);
            }).catch(e=>{
                socket.emit("registro2", false);
                throw e;
            });
        });
        socket.on("delete-user", async a=>{
            const {usuario,msg}=a;
            let {data}=await comprobar({usuario,msg})
                .catch(e=>{
                    socket.emit("delete-user2",false);
                    socket.emit("error",e);
                    return false;
                });
            if(data===false)return data;
            if(data!==usuario){
                socket.emit("delete-user2",false);
                return false;
            }
            let sqluser=SqlString.escape(usuario);
            let q1=`DELETE FROM short WHERE usuario=${sqluser}`;
            let q2=`DELETE FROM usuarios WHERE nombre=${sqluser}`;
            let pr=Promise.all([
                query(q1),
                query(q2)
            ]);
            return pr.then(()=>{
                socket.emit("delete-user2", true);
            });
        });
        socket.on("change-password", async a=>{
            const {msg}=a;
            const {data}=await desencriptar(msg);
            const {newKey, detachedSignature, user}=JSON.parse(data);
            if(user)var usuario=SqlString.escape(user);
            else return socket.emit("change-password2", {result: false, err: "User not specified"});
            let err;
            if(!newKey){
                err="No new key in message";
            }else{
                if(!detachedSignature&&!err)err="New key is not signed";
                else{
                    let res=await comprobar({msg: newKey, detachedSignature, usuario: user})
                        .then(()=>true)
                        .catch(e=>{
                            if(e.status=="invalid")return false;
                            throw e;
                        });
                    if(!res)err="Bad signature for key";
                    let keyCheck;
                    if(!err)keyCheck=await openpgp.key.readArmored(newKey)
                    if(!keyCheck.keys||keyCheck.keys.length<1)err=new Error("Invalid openpgp key");
                    if(keyCheck.err&&keyCheck.err[0])err=keyCheck.err[0];
                }
            }
            if(err)return socket.emit("change-password2", {result: false, err});
            query(`UPDATE usuarios SET privateKey='${newKey}' WHERE nombre=${usuario}`)
                .then(()=>{
                    return socket.emit("change-password2", {result: true, err}); // Here 'err' should be not defined or evaluated as false
                })
                .catch(e=>{
                    socket.emit("change-password2", {result: false, err: "Internal backend error!"});
                    throw e;
                })
        })
        socket.on("req-sync", async a=>{
            let usuario=SqlString.escape(a.msg.usuario);
            let q=await query(`SELECT nombre,publicKey,db FROM usuarios WHERE nombre=${usuario}`).then(b=>{
                return b.res[0];
            });
            if(!q){
                socket.emit("req-sync2",false);
                return socket.emit("direct", "sync_failed");
            }
            return encriptar({msg: Buffer.from(q.db||"", "base64").toString("utf8"), key:q.publicKey}).then(c=>{
                socket.emit("req-sync2", c.data);
            });
        });
        socket.on("sync", async a=>{
            let user=await sync(a, socket);
            let escaped=SqlString.escape(user);
            let key=await query(`SELECT publicKey from usuarios WHERE nombre=${escaped}`).then(b=>b.res[0]||{}).then(r=>r.publicKey);
            if(key){
                let msg=await encriptar({msg: user,key});
                return socket.broadcast.emit("refresh-sync", msg.data);
            }
        });
        socket.on("decidir_sync", a=>{
            decidir_sync(a).then(b=>{
                return socket.emit("decidir_sync2", b);
            }).catch(e=>{
                console.error("Ha sucedido un error al decidir la sincronización de la base de datos");
                socket.emit("decidir_sync2", false); //Stop sync
                socket.emit("direct", "sync_error");
                throw e;
            });
        });
        socket.on("is-new-key", a=>{
            const {usuario, msg}=a;
            comprobar({usuario,msg})
                .then(rt=>{
                    if(rt&&rt.data){
                        hasNewKey(usuario, rt.data)
                            .then(f=>socket.emit("is-new-key2", f))
                            .catch(e=>{
                                if(e.fatal)throw e;
                                return socket.emit("err", e);
                            });
                    }else{
                        return socket.emit("err", "Client must sign and send private key sha256");
                    }
                })
                .catch(e=>{
                    return socket.emit("err", e);
                });
        });
        socket.on("get-new-key", async a=>{
            const {usuario, msg}=a;
            let ok=await comprobar({usuario,msg}).then(rt=>rt.data==usuario).catch(e=>{
                socket.emit("err", e.toString?e.toString():e);
                return false;
            });
            if(!ok)return;
            let escaped=SqlString.escape(usuario);
            let q=`SELECT publicKey,privateKey from usuarios WHERE nombre=${escaped}`;
            return query(q).then(async r=>{
                let row=r.res[0];
                if(!row)return socket.emit("get-new-key2", {result: false, error: "Cannot get user entry"});
                let encKey=await encriptar({key: row.publicKey, msg: row.privateKey}).then(c=>c.data);
                let msg={result: true, key: encKey};
                return socket.emit("get-new-key2", msg);
            }).catch(e=>{
                console.error(e);
                socket.emit("get-new-key2", {result: false, error: "Internal server error"});
                throw e; //Rethrow error
            })
        })
        socket.on("get-link", async a=>{
            let ln=a.msg.link;
            if(ln.length!=6) return socket.emit("err", "Invalid identifier");
            let id=SqlString.escape(ln);
            return query(`SELECT * FROM short WHERE uid=${id}`).then(a=>{
                if(a.res.length<1){
                    socket.emit("err", "Identificador incorrecto. Es posible que se haya eliminado");
                    socket.emit("get-link2", false);
                    return;
                }
                return socket.emit("get-link2", a.res[0].link);
            });
        });
        socket.on("set-public", a=>{
            desencriptar(a.msg).then(b=>{
                let x=JSON.parse(b.data);
                comprobar({usuario: x.usuario, msg: x.data.data}).then(c=>{
                    let d=JSON.parse(c.data);
                    let dat={};
                    for(let i in d){
                        if(!d.hasOwnProperty(i))continue;
                        dat[i]=SqlString.escape(d[i]);
                    }
                    query(`INSERT INTO short (link,uid,usuario) VALUES (${dat.link},${dat.uid},${dat.usuario})`).then(()=>{
                        socket.emit("set-public2", {status: true});
                    }).catch(e=>{
                        socket.emit("set-public2", {status: false, err: e});
                    });
                });
            });
        });
        socket.on("update-public", a=>{
            desencriptar(a.msg).then(b=>{
                let x=JSON.parse(b.data);
                comprobar({usuario: x.usuario, msg: x.data.data}).then((c,d=JSON.parse(c.data))=>{
                    let link=SqlString.escape(d.link);
                    let uid=SqlString.escape(d.uid);
                    query(`UPDATE short SET link=${link} WHERE uid=${uid}`).then(()=>{
                        socket.emit("update-public2", {status: true});
                    }).catch(e=>{
                        socket.emit("update-public2", {status: false, err: e});
                    });
                });
            });
        });
        socket.on("del-public", a=>{
            desencriptar(a.msg).then(b=>{
                let x=JSON.parse(b.data);
                comprobar({usuario: x.usuario, msg: x.data.data}).then((c, d=JSON.parse(c.data))=>{
                    let uid=SqlString.escape(d.uid);
                    query(`DELETE FROM short WHERE uid=${uid}`).then(()=>{
                        socket.emit("del-public2", {status: true});
                    }).catch(e=>{
                        socket.emit("update-public2", {status: false, err: e});
                    });
                });
            });
        });
    });
})();

//-------------------------------------
// UTILITIES
//-------------------------------------

async function hasNewKey(user, sum){
    let username=SqlString.escape(user);
    let {checksum,checksum2}=await query(`SELECT SHA2(privateKey, '256') AS checksum, SHA2(REPLACE(privateKey, '\r', ''), '256') as checksum2 FROM usuarios WHERE nombre=${username}`)
        .then(f=>{
            if(!f.res.length>0)return;
            return f.res[0];
        });
    if(checksum!=sum){
        if(checksum2!=sum)return true;
    }
    return false;
}

function decidir_sync(a){
    return query("SELECT SHA2(db, '256') as checksum,modificado FROM usuarios WHERE nombre='"+a.msg.usuario+"'").then(b=>{
        let sSha=b.res[0].checksum;
        let res=b.res[0];
        if(sSha==a.msg.db){
            return false;
        }
        let fechaSrv=res.modificado;
        if(fechaSrv>a.msg.fecha){
            return "servidor";
        }else{
            return "cliente";
        }
    });
}
async function sync(a, socket){
    try{
        let c=await desencriptar(a.msg).then(t=>JSON.parse(t.data));
        return await comprobar({msg: c.db, usuario: c.usuario}).then(async d=>{
            if(d==false){
                socket.emit("direct", "pgp_sign_check_nok");
                socket.emit("sync2", {error: true, msg: "pgp_sign_check_nok"});
                throw new Error("No hemos podido verificar que la base de datos sea de tu propiedad");
            }
            let fecha=SqlString.escape(c.fecha);
            let usuario=SqlString.escape(c.usuario);
            try{
                await query(`UPDATE usuarios SET db='${Buffer.from(d.data).toString("base64")}',modificado=${fecha} WHERE nombre=${usuario}`);
                socket.emit("sync2", true);
                return c.usuario;
            }catch(e){
                e.internalError=true;
                throw e;
            }
        }).catch(e=>{
            socket.emit("direct", "pgp_sign_check_nok");
            socket.emit("sync2", {error: true, msg: "pgp_sign_check_nok"});
            const {message,stack}=e;
            socket.emit("debug", JSON.stringify({message,stack}));
            if(e.internalError)throw e;
        });
    }catch(e){
        socket.emit("sync2", false);
        socket.emit("error", e);
        if(e.internalError)throw e;
    }
}
async function comprobar(a){
    let usuario=SqlString.escape(a.usuario);
    let res=await query(`SELECT publicKey,nombre FROM usuarios WHERE nombre=${usuario}`).then(b=>{
        return b.res.filter(c=>c.nombre===a.usuario)[0];
    });
    if(!res){
        throw new Error("No hemos podido encontrar el usuario ni su clave");
    }
    return openpgp.verify({
        message: a.detachedSignature?await openpgp.cleartext.fromText(a.msg):await openpgp.message.readArmored(a.msg),
        signature: a.detachedSignature?await openpgp.signature.readArmored(a.detachedSignature):undefined,
        publicKeys: (await openpgp.key.readArmored(res.publicKey)).keys
    }).then(async d=>{
        let buff=await openpgp.stream.readToEnd(d.data);
        let data=new StringDecoder("utf-8").write(buff);
        if(await d.signatures[0].verified){
            return {data};
        }else{
            let e=new Error("Invalid signature for message");
            e.data=data;
            e.status="invalid";
            throw e;
        }
    }).catch(e=>{
        e.internalError=true;
        throw e;
    });
}
async function encriptar(a){
    return openpgp.encrypt({
        message: await openpgp.message.fromText(a.msg),
        publicKeys: (await openpgp.key.readArmored(a.key)).keys
    });
}
async function desencriptar(a){
    return openpgp.decrypt({
        message: await openpgp.message.readArmored(a),
        privateKeys: pgpObj.privada
    });
}

//Slack integration
var sendSlack;
(async function(){
    await credsProgress;
    if(creds.slack){
        //We can send errors to some Slack channel
        const webhook=new IncomingWebhook(creds.slack.url);
        sendSlack=msg=>{
            return new Promise((resolver,rechazar)=>{
                webhook.send(msg,e=>{
                    if(e){
                        e.fromSlack=true;
                        return rechazar(e);
                    }else{
                        return resolver(msg);
                    }
                });
            });
        }
    }
})();

function stop(code){ //Function to exit the backend in pace
    if(typeof io!="undefined"&&io.close){
        Console.log("Closing socket.io server");
        io.close();
    }
    if(typeof server!="undefined"){
        Console.log("Closing HTTP server");
        server.close();
    }
    if(typeof pool!="undefined"){
        Console.log("Stopping MySQL pool");
        pool.end();
    }
    if(code)exit(code,true);
}
function exit(code=0,lastInstance){
    if(!lastInstance)return stop(code);
    Console.log("Exiting backend on code "+code);
    process.exit(code);
}

//Catch errors from app
async function unhandledRejectionHandler(e,p){
    if(typeof sendSlack != "undefined"){
        let msg="Backend error! Undefined error";
        if(e)msg=`Backend error! ${e} ${e.stack ? '\n ```'+e.stack+'```' : " but there's no trace"}`;
        if(e&&!e.fromSlack||!e)await sendSlack(msg);
    }
    if(process.env.NODE_ENV=="test")e.fatal=true;
    if(!e.fatal){
        Console.log("Handling a unhandled Promise rejection at ",p);
    }else{
        console.error(p);
        return exit(typeof e.code=="number"?e.code:100);
    }
}
function unhandledExceptionHandler(err){
    console.error(err);
    if(err.fatal){
        return exit(1);
    }
    Console.log("Exiting due to an uncaught error");
    exit(128);
}
process.on("unhandledRejection", unhandledRejectionHandler);
process.on("uncaughtException", unhandledExceptionHandler);

//Catch signals sent to the process
process.on("SIGINT", ()=>exit(0));
process.on("SIGTERM", ()=>exit(0));

//Catch the exit of the backend
process.on("exit", code=>{
    exit(code,true);
});
if(process.send){
    portListenProgress.then(()=>{
        process.send("ready");
    });
}
module.exports={stop,portListenProgress};
