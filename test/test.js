/*eslint-env node, mocha*/
/*eslint-disable prefer-arrow-callback*/
const {codeTest,moduleTest}=require("../test-module");
const {fork}=require("child_process");
const assert=require("assert");
const exec=require("util").promisify(require("child_process").exec);
const path=require("path");
const http=require("http");
const crypto=require("crypto");
const io=require("socket.io-client");
const fs=require("fs");
const openpgp=require("openpgp");

process.env.NODE_ENV='test';

beforeEach(function(){
    this.timeout(0); //Disable timeout
});

it("modules should be installed",function(){
    return moduleTest(false);
});
it("should pass basic test",function(){
    let patterns={
        JS: /^.*\.js$/,
        JSON: /^.*\.json$/
    };
    let directories=[path.resolve(".")];
    return codeTest(patterns,directories,false);
});
it("sould pass eslint",function(){
    return exec("eslint .");
});
describe("Backend functions", function(){
    if(!process.env.PORT){
        process.env.PORT=8080;
    }
    var backend=fork("main.js");
    backend.on("close", code=>{
        if(code!==0)throw new Error("Backend closed with code "+code);
    });
    const portListenProgress=new Promise(r=>{
        backend.on("message",name=>{
            if(name=="ready")return r();
        });
    });
    it("Should bind port "+process.env.PORT, async function(){
        this.timeout(10000);
        let url=`http://localhost:${process.env.PORT}`;
        await portListenProgress;
        return new Promise((resolve,reject)=>{
            const req=http.request(url,res=>{
                const {statusCode}=res;
                if(statusCode==200)return resolve(200);
                return reject(new Error("Server returned "+statusCode));
            }).on("error", e=>{
                reject(e);
            });
            req.end();
            return req;
        });
    });
    describe("Test socket", function(){
        this.timeout(4000);
        var newKey,privkey,pubkey,serverKey;
        pubkey=fs.readFileSync(path.join(__dirname,"pubKey"),"UTF-8");
        privkey=fs.readFileSync(path.join(__dirname,"privKey"),"UTF-8");

        const user="'; DROP table Claves"; //Should register also rare usernames
        const password="testuserkey";
        const newpassword="testuserkeynew";

        const socket=io("http://localhost:"+process.env.PORT, {autoConnect: false});
        let errList=["error","connect_error"];
        const sharedLink={
            url: "https://vikserver.tk",
            id: "54a341"
        };
        errList.forEach(er=>{
            socket.on(er,e=>{
                throw new Error(e);
            });
        });
        it("Should send a PGP key", function (done){
            socket.on("pgp", res=>{
                openpgp.key.readArmored(res.key)
                .then(r=>{
                    if(r.err){
                        let e=new Error(r.err);
                        e.fatal=true;
                        throw e;
                    }
                    serverKey=r.keys;
                })
                .then(()=>done());
            });
            socket.open();
        });
        it("User check should send a boolean", function(done){
            socket.on("chk2", resp=>{
                socket.off("chk2");
                if(resp===true||resp===false)return done();
                throw new Error(`Expected a boolean but returned ${resp} of type ${typeof resp}`);
            });
            socket.emit("chk", {msg:"unknownuser"});
        });
        it("Should register a new user", async function(){
            let pr=new Promise((resolve,reject)=>{
                socket.on("registro2", result=>{
                    if(!result)return reject(new Error("Backend returned "+result+" but expected true"));
                    return resolve();
                });
            });
            let msg={
                creds:{
                    usuario:user
                },
                keys:{
                    pública: pubkey,
                    privada: privkey
                }
            };
            let message=await openpgp.message.fromText(JSON.stringify(msg));
            let enc=await openpgp.encrypt({message,publicKeys: serverKey});
            socket.emit("registro",{msg:enc.data});
            return pr;
        });
        it("Should recognize that user", function(done){
            socket.on("chk2", res=>{
                socket.off("chk2");
                if(res)return done();
                throw new Error("User was not in database. Expected true but received "+res);
            });
            socket.emit("chk",{msg:user});
        });
        it("Must not return user key", async function(){
            let pr=new Promise((resolve,reject)=>{
                return socket.on("req-pgp2", res=>{
                    socket.off("req-pgp2");
                    if(res==false)return resolve();
                    return reject(new Error("Openpgp key granted without a valid password"));
                });
            });
            let msg={
                usuario: user,
                password: ""
            };
            let message=await openpgp.message.fromText(JSON.stringify(msg));
            let enc=await openpgp.encrypt({message,publicKeys: serverKey});
            socket.emit("req-pgp",{msg:enc.data});
            return pr;
        })
        it("Should return the user key", async function(){
            let pr=new Promise((resolve,reject)=>{
                return socket.on("req-pgp2",res=>{
                    socket.off("req-pgp2");
                    if(res==false)return reject(new Error("Openpgp key denied"));
                    return resolve();
                });
            });
            let msg={
                usuario: user,
                contraseña: password
            };
            let message=await openpgp.message.fromText(JSON.stringify(msg));
            let enc=await openpgp.encrypt({message,publicKeys: serverKey});
            socket.emit("req-pgp",{msg:enc.data});
            return pr;
        });
        it("User can share links", async function(){
            let pr=new Promise((resolve,reject)=>{
                return socket.on("set-public2", res=>{
                    if(res.status)return resolve(true);
                    return reject(res.err);
                });
            });
            let msg={
                link: sharedLink.url,
                uid: sharedLink.id,
                usuario: user
            };
            let message=await openpgp.message.fromText(JSON.stringify(msg));
            let privateKeys=(await openpgp.key.readArmored(privkey)).keys[0];
            await privateKeys.decrypt(password);
            let signed=await openpgp.sign({message,privateKeys});
            let nd={
                usuario: user,
                data:signed
            };
            let nm=await openpgp.message.fromText(JSON.stringify(nd));
            let enc=await openpgp.encrypt({message:nm,publicKeys:serverKey});
            socket.emit("set-public", {msg:enc.data});
            return pr;
        });
        it("Should return the shared link", function(done){
            socket.on("get-link2",res=>{
                socket.off("get-link2");
                if(res==false)throw new Error("Link doesn't exist");
                if(res!=sharedLink.url)throw new Error("Link doesn't match the shared one");
                return done();
            });
            socket.emit("get-link", {msg:{link:sharedLink.id}});
        });
        it("Should delete shared link", async function(){
            let uid="asd32d";
            let msg={
                link: "https://duckduckgo.com",
                uid,
                usuario: user
            };
            let message=await openpgp.message.fromText(JSON.stringify(msg));
            let privateKeys=(await openpgp.key.readArmored(privkey)).keys[0];
            await privateKeys.decrypt(password);
            let signed=await openpgp.sign({message,privateKeys});
            let nd={
                usuario: user,
                data:signed
            };
            let nm=await openpgp.message.fromText(JSON.stringify(nd));
            let enc=await openpgp.encrypt({message:nm,publicKeys:serverKey});
            let pr1=new Promise(r=>{
                socket.on("set-public2",res=>{
                    let rt=assert.deepEqual(res.status,true);
                    return r(rt);
                });
            });
            socket.emit("set-public",{msg:enc.data});
            await pr1;
            let pr2=new Promise(r=>{
                socket.on("del-public2",res=>{
                    let rt=assert.deepEqual(res.status,true);
                    return r(rt);
                })
            })
            socket.emit("del-public",{msg:enc.data});
            await pr2;
            socket.off("set-public2");
            socket.off("del-public2");
            let pr3=new Promise(r=>{
                socket.on("get-link2",res=>{
                    socket.off("get-link2");
                    let rt=assert.deepEqual(res,false);
                    return r(rt);
                });
            });
            socket.emit("get-link",{msg:{link:uid}});
            return pr3;
        });
        it("There's no new key", async function(){
            let pr=new Promise((r,j)=>{
                socket.on("is-new-key2",res=>{
                    if(res)j(new Error(`No rekey needed but backend returned ${res}`));
                    else r();
                    socket.off("is-new-key");
                });
                socket.on("err", e=>j(e));
            });
            pr.finally(()=>{
                socket.off("is-new-key2");
                socket.off("err");
            });
            let sum=await genSHA(privkey);
            let privateKeys=(await openpgp.key.readArmored(privkey)).keys[0];
            await privateKeys.decrypt(password);
            let msg=await signMessage(privateKeys, sum);
            socket.emit("is-new-key", {usuario: user, msg});
            return pr;
        });
        it("Should be able to change key", async function(){
            let privateKeys=(await openpgp.key.readArmored(privkey)).keys[0];
            await privateKeys.decrypt(password);
            await privateKeys.encrypt(newpassword);
            await privateKeys.decrypt(newpassword);
            newKey=privateKeys.armor();
            let detachedSignature=await detachedSign(privateKeys, newKey);
            let msg=await encrypt(serverKey, {newKey, detachedSignature, user});
            let pr=new Promise((r,j)=>{
                socket.on("change-password2", res=>{
                    if(!res.result||res.err)return j(res.err);
                    return r(res);
                });
            });
            pr.finally(()=>socket.off("change-password2"));
            socket.emit("change-password", {msg});
            return pr;
        });
        it("New key available", async function(){
            let pr=new Promise((r,j)=>{
                socket.on("is-new-key2",res=>{
                    if(!res)j(new Error(`Rekey needed but backend returned ${res}`));
                    else r();
                    socket.off("is-new-key");
                });
                socket.on("err", e=>j(e));
            });
            pr.finally(()=>{
                socket.off("is-new-key2");
                socket.off("err");
            });
            let sum=await genSHA(privkey);
            let privateKeys=(await openpgp.key.readArmored(privkey)).keys[0];
            await privateKeys.decrypt(password);
            let msg=await signMessage(privateKeys, sum);
            socket.emit("is-new-key", {usuario: user, msg});
            return pr;
        });
        it("New key equals local key", async function(){
            async function checkResponse(response){
                let {result, key, error}=response;
                if(result!==true||error){
                    throw error;
                }
                let decKey=await openpgp.decrypt({
                    privateKeys,
                    message: await openpgp.message.readArmored(key)
                })
                    .then(b=>b.data)
                let nksha=await genSHA(newKey.replace(/\r/gm, ""));
                let dksha=await genSHA(decKey.replace(/\r/gm, ""));
                return assert.strictEqual(nksha, dksha);
            }
            let pr=new Promise((r,j)=>{
                socket.on("get-new-key2", res=>{
                    checkResponse(res).then(r).catch(j);
                });
                socket.on("err", e=>j(e));
            });
            pr.finally(()=>{
                socket.off("err");
                socket.off("get-new-key2");
            });
            let privateKeys=await openpgp.key.readArmored(newKey).then(r=>r.keys[0]);
            let msg=await signMessage(privateKeys, user);
            socket.emit("get-new-key", {usuario: user, msg});
            return pr;
        })
        it("Should delete user", async function(){
            let pr=new Promise((resolve,reject)=>{
                return socket.on("delete-user2", res=>{
                    if(res)return resolve();
                    return reject(new Error("Backend didn't remove user"));
                });
            });
            let message=await openpgp.message.fromText(user);
            let privateKeys=(await openpgp.key.readArmored(privkey)).keys[0];
            await privateKeys.decrypt(password);
            let signed=await openpgp.sign({message,privateKeys});
            socket.emit("delete-user",{msg: signed.data,usuario:user});
            return pr;
        });
        it("Shared link no longer accesible", function(){
            let pr=new Promise((r,j)=>{
                socket.on("get-link2",res=>{
                    socket.off("get-link2");
                    if(res==false)return r();
                    return j(new Error(`Backend returned ${res} instead of false`));
                });
            });
            socket.emit("get-link",{msg:{link:sharedLink.id}});
            return pr;
        })
        it("Should stop", function(done){
            socket.close();
            socket.destroy();
            backend.on("exit", done);
            backend.kill('SIGTERM');
        });
    });
})

async function signMessage(privateKeys, msg){
    //The privateKeys parameter should be decrypted and message a buffer or a string
    let message=await openpgp.message.fromText(msg);
    return (await openpgp.sign({message, privateKeys})).data;
}

async function genSHA(message){
    return crypto.createHash("sha256").update(message, "utf-8").digest().toString("hex");
}

function detachedSign(privateKey, msg){
    let message=openpgp.message.fromText(msg);
    return openpgp.sign({
        message,
        privateKeys: [privateKey],
        detached: true
    }).then(res=>{
        return res.signature;
    });
}

async function encrypt(publicKey, msg){
    let message=openpgp.message.fromText(typeof msg=="object"?JSON.stringify(msg):msg);
    let publicKeys;
    if(typeof publicKey == "string"){
        publicKeys=await openpgp.key.readArmored(publicKey).then(r=>r.keys);
    }else{
        publicKeys=publicKey;
    }
    return openpgp.encrypt({
        message,
        publicKeys
    }).then(t=>{
        return t.data;
    });
}
